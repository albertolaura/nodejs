
module.exports = {
    app: require ('./app'),
    db: require('./db'),
    constants: require('./constants')
};