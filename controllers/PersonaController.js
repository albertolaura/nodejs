
const { app, constants } = require('../config');
 
const { PersonaModel } = require('../models');

const fs= require('fs');
const ejs = require('ejs');
const pdf = require('html-pdf');

const axios = require('axios');

const listarPersonas = async ( req, res ) => {

    const listaPersonas = await PersonaModel.find();

    const html = fs.readFileSync('./views/listarPersonas.ejs', 'utf8');
    const htmlRender = ejs.render(html, {listaPersonas} );
    res.send(htmlRender);

};


const registrarPersona = async (req, res) => {

    res.render('registrarPersona.ejs');
}

const adicionarPersona = async (req, res) => {

    const datos = req.body;
    const personaCreada = await PersonaModel.create(datos);
    
    res.redirect ('/listar');

}

const eliminarPersona = async (req, res) => {
    try {
        const personaEliminada = await PersonaModel.findByIdAndDelete(req.params.id);
       
        if (!personaEliminada) {
            return res.status(404).send();
        }
        
        res.redirect ('/listar');

    } catch (error) {
        res.status(500).send(error);
    }
}

const editarPersona = async (req, res) => {

    try{

        const editaPersona = await PersonaModel.findById(req.params.id);

        if (!editarPersona) {
            return res.status(404).send();
        }

        res.render('editarPersona.ejs', {persona: editaPersona } );

    } catch (error) {

    }

}

const actualizaPersona = async (req, res) => {
    try {
        const personaActualizada = await PersonaModel.findByIdAndUpdate(req.params.id, req.body, {new: true});

        console.log('Persona Actualizada= ', personaActualizada);

        if (!personaActualizada) {
            return res.status(404).send();
        }

        res.redirect ('/listar');

    } catch (error) {
        res.status(500).send(error);
    }
}


const generaPdf = async (req, res) => {

    const options = { format: 'Letter'} 

    const listaPersonas = await PersonaModel.find();

   const html = fs.readFileSync('plantilla1.ejs', 'utf8');
   const htmlRender = ejs.render(html, {listaPersonas} );

   pdf.create(htmlRender, options).toFile('personas.pdf', 
   function (error, response)  {
       if (error) {
           console.log(error);
       }

       //res.send(htmlRender);

       console.log('****', response);

       var tempFile='personas.pdf';
       fs.readFile(tempFile, function (err,data){
          res.contentType("application/pdf");
          res.send(data);
       });
    
    
   })

   
}



const generarToken = (req, res) => {
   
    console.log('constant=', constants);

    res.status(200).json({
        finalizado: true,
        mensaje: 'Token generado correctamente',
        datos: `e0101 TIEMPO ${app.expirationToken} `
    })
}



module.exports = {
    listarPersonas,
    generarToken, 
    registrarPersona,
    adicionarPersona,
    eliminarPersona,
    editarPersona, 
    actualizaPersona,
    generaPdf
}