const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require ('body-parser');


        // conexion
const mongoose = require('mongoose');       //db:mongoose
const {db} = require('./config');

//mongoose.connect('mongodb://localhost:27017/clase-final', { useNewUrlParser: true, useUnifiedTopology: true } );
mongoose.connect( db.urlConexion , { useNewUrlParser: true, useUnifiedTopology: true } );

app.use(express.urlencoded({extended:false}));          // para POST
app.use(bodyParser.json());                             // para formato JSON
//app.use(express.json());


const { PersonaRoute } = require( './routes');

	// http://localhost:3000/personas

app.use(PersonaRoute);


app.listen(port, () => {
    console.log( ` Funcionando, puerto:  ${port}` );
})
