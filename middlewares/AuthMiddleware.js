
const verificarToken = ( req, res, next) => {
    // Peticiones-> token -> 
    // cabecera

    try{
        const {authorization} = req.headers;
        console.log(' alc: Desde middleware ..');
        //console.log('authorization = ', authorization);
        console.log(req.headers);
                                            // En Postman: Authorization cambia a authorization
        if (authorization !== 'TOKEN') {      // desde Variable POSTMAN ingresar: Header
            throw new Error ('No autorizado');
        }

        req.nombreCompleto = 'Ivan Tancara' // Enviando a siguiente nivel

        next();

    } catch (error) {
        res.status(401).json({
            finalizado: false, 
            mensaje: error.message, 
            datos: null
        })
        console.log('ALC Middleware: error');
    }


}

module.exports = {
    verificarToken
}