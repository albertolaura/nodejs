
var mongoose = require('mongoose');
var { Schema } = mongoose;

var PersonaSchema = new Schema({
    nombres: String, 
    apellidoPaterno: String, 
    apellidoMaterno: String, 
    numeroDocumento: Number
});

const Persona = mongoose.model('persona', PersonaSchema );

module.exports = Persona; 