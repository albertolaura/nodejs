const express = require('express');
const router = express.Router();

const { PersonaController } = require( '../controllers' );

const { AuthMiddleware} = require('../middlewares');

//router.get('/personas', AuthMiddleware.verificarToken, PersonaController.listarPersonas );

//http://localhost:3000/persona  -- Body/raw/json
//{
//    "nombres": "Ivan",
//    "apellidoPaterno": "Tancara", 
//    "apellidoMaterno": "Casilla", 
//    "numeroDocumento": 32132
//}

// levantando mongodb
//  sudo systemctl start mongod.service	
//	sudo systemctl status mongod

//  Lista las personas
//  http://localhost:3000/listar

router.get('/', PersonaController.listarPersonas );

router.get('/listar', PersonaController.listarPersonas );

// Adiciona una persona
//  http://localhost:3000/registrar
router.get('/registrar', PersonaController.registrarPersona);

router.post('/adicionar', PersonaController.adicionarPersona);

// Actualiza una persona
//  http://localhost:3000/editar/611688c2bb53b45285ac6a8b
router.get('/editar/:id', PersonaController.editarPersona);

//  http://localhost:3000/actualiza/611688c2bb53b45285ac6a8b
router.post('/actualiza/:id', PersonaController.actualizaPersona);

//  Elimina una persona
//  http://localhost:3000/eliminar/61168c951bc3ac5870213037
router.get('/eliminar/:id', PersonaController.eliminarPersona);

//  Lista las personas
//  http://localhost:3000/pdf
router.get('/generaPdf', PersonaController.generaPdf );


module.exports = router;

